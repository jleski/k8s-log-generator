package main

import (
	"flag"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
	"gopkg.in/natefinch/lumberjack.v2"
)

type (
	Settings struct {
		LogInterval int
		LogFile     string
	}
	LogOutput struct {
		logger    *logrus.Logger
		fields    *logrus.Fields
		formatter *logrus.TextFormatter
	}
)

func getNewRandomJoke() string {
	req, err := http.NewRequest("GET", "https://icanhazdadjoke.com", nil)
	if err != nil {
		log.Fatalln("Error: Unable to prepare HTTP request:", err)
	}
	client := &http.Client{
		Timeout: 15 * time.Second,
	}
	req.Header.Set("Accept", "text/plain")
	resp, reqErr := client.Do(req)
	if reqErr != nil {
		log.Fatalln("Error: Unable to send HTTP request:", reqErr)
	}
	body, readErr := ioutil.ReadAll(resp.Body)
	if readErr != nil {
		log.Fatalln("Error: Unable to read HTTP response:", readErr)
	}
	return string(body)
}

func log_generic(output *LogOutput) {
	output.logger.SetLevel(logrus.TraceLevel)
	output.logger.WithFields(*output.fields).Info(getNewRandomJoke())
}

// Start logging every _interval_ seconds
func log_with_interval(settings *Settings) {
	logrus.SetFormatter(&logrus.TextFormatter{TimestampFormat: "2006-01-02 15:04:05", FullTimestamp: true})
	duration := time.Duration(settings.LogInterval * int(time.Second))

	hostname, ok := os.LookupEnv("HOSTNAME")
	if !ok {
		hostname = "unset"
	}

	// Initialize loggers
	fileOutput := &LogOutput{
		logger: logrus.New(),
		fields: &logrus.Fields{
			"Source":     hostname,
			"SourceType": "audit",
			"EventType":  "privilege",
		},
		formatter: &logrus.TextFormatter{TimestampFormat: "2006-01-02 15:04:05", FullTimestamp: true, DisableColors: true},
	}
	consoleOutput := &LogOutput{
		logger: logrus.New(),
		fields: &logrus.Fields{
			"Source":     "https://icanhazdadjoke.com",
			"SourceType": "api",
			"EventType":  "getNewRandomJoke",
		},
		formatter: &logrus.TextFormatter{TimestampFormat: "2006-01-02 15:04:05", FullTimestamp: true},
	}
	if settings.LogFile != "" {
		fileOutput.logger.SetOutput(&lumberjack.Logger{
			Filename:   settings.LogFile,
			MaxSize:    3, // megabytes
			MaxBackups: 1,
			MaxAge:     7,    //days
			Compress:   true, // disabled by default
		})
		fileOutput.logger.SetFormatter(fileOutput.formatter)
	}
	consoleOutput.logger.SetOutput(os.Stdout)
	consoleOutput.logger.SetFormatter(consoleOutput.formatter)

	for range time.Tick(duration) {
		log_generic(consoleOutput)
		if settings.LogFile != "" {
			log_generic(fileOutput)
		}
	}
}

// Wrapper for the actual mock logger functionality
func start_logging(settings *Settings) {
	go log_with_interval(settings)
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	// Block until signal (os.Interrupt)
	<-c
}

func get_default_settings() Settings {
	return Settings{LogInterval: 5}
}

// Entry
func main() {
	settings := get_default_settings()
	flag.IntVar(&settings.LogInterval, "interval", settings.LogInterval, "Interval defining how often a mock log entry is created")
	flag.StringVar(&settings.LogFile, "logfile", settings.LogFile, "Optional. Logfile")
	flag.Parse()
	http.Handle("/metrics", promhttp.Handler())

	go func() {
		srv := &http.Server{
			ReadTimeout:  5 * time.Second,
			WriteTimeout: 10 * time.Second,
			Addr:         ":9090",
		}
		log.Println(srv.ListenAndServe())
	}()

	start_logging(&settings)
}
