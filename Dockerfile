# Use go alpine image as build stage
FROM golang:1.20-alpine as build
WORKDIR /build
COPY . .
RUN go mod download
# Build our executable.
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-s -w" -o loggen_linux .
FROM alpine:3
# Copy our static executable.
COPY --from=build /build/loggen_linux /bin/loggen
RUN chmod +x /bin/loggen && mkdir -p /app
WORKDIR /app
# Add new user
RUN adduser -h /home/appuser -D appuser \
 && chown appuser .
USER appuser
# Run the binary.
CMD ["loggen"]
