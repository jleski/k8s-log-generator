module gitlab.com/jleski/k8s-log-generator

go 1.16

require (
	github.com/prometheus/client_golang v1.14.0 // indirect
	github.com/sirupsen/logrus v1.9.0
	go.opentelemetry.io/contrib/instrumentation/net/http/httptrace/otelhttptrace v0.40.0 // indirect
	go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp v0.40.0 // indirect
	go.opentelemetry.io/contrib/instrumentation/runtime v0.40.0 // indirect
	go.opentelemetry.io/otel v1.14.0 // indirect
	go.opentelemetry.io/otel/exporters/prometheus v0.37.0 // indirect
	golang.org/x/sys v0.7.0 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.2.1
)
